import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container } from '@chakra-ui/react';
import { Box } from '@chakra-ui/react';
import { getBoards,deleteBoard } from '../services/AxiosAPI';
import { useState } from 'react';
import { Image } from '@chakra-ui/react'
import createBoard from '../services/AxiosAPI';
import { Input } from '@chakra-ui/react'
import { Button, ButtonGroup } from '@chakra-ui/react'

let imageUrl ="https://trello-backgrounds.s3.amazonaws.com/SharedBackground/480x320/fde291621e297defb5fe147745217e73/photo-1680761060530-87f01f1f1803.jpg"

const Mainbar = () => {
    const [boardData, setBoardData] = useState(null);
    const [query, setQuery] = useState("")
    
    useEffect(() => {
        getBoards().then(response => setBoardData(response));
    }, [boardData])
    

    function handleSubmit(event) {
        event.preventDefault();
        createBoard(query);
        setQuery("")
    }

    function handleChange(event) {
        setQuery(event.target.value)
    }

    function handleDelete(id ) {
        deleteBoard(id)
}

  return (
      <div>
          {/* <Link to="/board"><button>See every Board here</button></Link> */}
          <form onSubmit={handleSubmit}>
              <Input placeholder='Basic usage'onChange={handleChange}/>
              <Button colorScheme='blue'>Create Board</Button>
          </form>
          <div className="container-fluid d-flex">
              {boardData?
                  (boardData.map((board, index) => {
                      return (
                          <div className="card text-bg-dark w-25 m-3" key={index+board.name}>
                                                <img src={imageUrl} className="card-img" alt="Board" />
                                            <div className="card-img-overlay">
                                  <h5 className="card-title">{board.name}</h5>
                                  <button onClick={() => handleDelete(board.id)}>delete</button>
                                            </div>
                                    </div>

                                )

                  })
                  ):""
              }

          </div>
      </div>
  )
}

export default Mainbar

// return (
//     <div key={index} >
//         <Image
//             boxSize='250px'
//             src='https://trello-backgrounds.s3.amazonaws.com/SharedBackground/480x320/fde291621e297defb5fe147745217e73/photo-1680761060530-87f01f1f1803.jpg'
//             alt="board image"
//         />
//     </div>
// )