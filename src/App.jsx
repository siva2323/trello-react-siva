import Navbar from "./components/Navbar"
import Mainbar from "./components/Mainbar"
import { BrowserRouter } from "react-router-dom"
import { Route } from "react-router-dom"
import { Routes } from "react-router-dom"
import { Link } from "@chakra-ui/react"
import Board from"./components/Board"

function App() {

  return (
    <div className="App">
      <Navbar />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Mainbar />} />
          {/* <Route path="/board" element={<Board />} /> */}
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
